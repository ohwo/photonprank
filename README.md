#Photon Prank
This was a gift I gave to my friends at my last day of working for the Photon Factory. Basically you install it on your friends computers, and it will schedule itself to run each day at a specified period via the windows task scheduler. 

* The first time the program runs it sets itself up via task scheduler - this is sort of the install step (see step 2 of "Testing the app on your computer" below).
* Each time afterwards the program will download the "first" image it finds in your app's dropbox folder and set this image as the background image of that computer. 
* You can use any image, I used images of myself :D 


## Configuring, compiling and installing on your friends computers
Unfortunately there are a few steps in geting this program to work. 

Basically you have to configure the program to work with your dropbox account and then compile it. 
Then you can go and install the app on all your friends computers. 

All this could be made a whole bunch simpler and if I get some time I will try to make some kind of user interface. Think of this stuff as a temporary fix.. 

### To setup the program with your dropbox account
1. Go to https://www.dropbox.com/developers/apps/create
![dropbox01.png](https://bitbucket.org/repo/a98qRB/images/3390911956-dropbox01.png)
2. Choose "Dropbox API app"
3. Set app to be limited to its own folder
4. Go to your new app console: https://www.dropbox.com/developers/apps
5. Click on the title of your new app to edit details
![dropbox02.png](https://bitbucket.org/repo/a98qRB/images/1365779879-dropbox02.png)
6. a. You can leave most things as is - development status is fine for these little pranks. 
   b. You will however need to copy and paste your:
      i.  app key 
      ii. app secret
     iii. app access token
![dropbox05.png](https://bitbucket.org/repo/a98qRB/images/657069113-dropbox05.png)
7. Open visual studio as administrator, and then open the program

![dropbox00.png](https://bitbucket.org/repo/a98qRB/images/2545735405-dropbox00.png)
8. Paste the app key, secret and access token into the visual studio program as _consumerKey, _consumerSecret and _accessToken respectively. 

![dropbox04.png](https://bitbucket.org/repo/a98qRB/images/2835579318-dropbox04.png)

### Compiling the app
1. Right click on the project and choose publish
![dropbox06.png](https://bitbucket.org/repo/a98qRB/images/1020262689-dropbox06.png)
2. Choose a publish directory and click publish
![dropbox07.png](https://bitbucket.org/repo/a98qRB/images/881863797-dropbox07.png)
3. Done!! 

### Testing the app on your computer (or installing it on your friends)
1. Save an image to your new dropbox app folder (Dropbox > Apps > [PhotonPrank] > [image])
![dropbox09.png](https://bitbucket.org/repo/a98qRB/images/202840004-dropbox09.png)
2. Right click the setup file and click run as administrator. 
![dropbox08.png](https://bitbucket.org/repo/a98qRB/images/52939740-dropbox08.png)
3. Your app will install but your desktop wont change until the next time the app runs.
You can force it to change by running the setup.exe again (step 2). Otherwise it will run as scheduled.